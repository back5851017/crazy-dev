<?php

namespace App\Controller;

use App\Entity\Extraterrestre;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class AlienListController extends AbstractController
{
    private $session;

    public function __construct(private RequestStack $requestStack,)
    {
        $this->session = $requestStack->getSession();
    }

    #[Route('/alien_list', name: 'alien_list')]
    public function show(EntityManagerInterface $entityManager): Response
    {
        if ($this->session->get('log') == 'true') {
            $aliens = $entityManager->getRepository(Extraterrestre::class)->findAll();
            return $this->render('pages\aliens\aliens.html.twig', ['aliens' => $aliens]);
        } else {
            return $this->redirectToRoute('login');
        }
    }
}

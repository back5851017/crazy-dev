<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;


class CartController extends AbstractController
{
    private $session;

    public function __construct(private RequestStack $requestStack,)
    {
        $this->session = $requestStack->getSession();
    }

    #[Route('/cart', name: 'cart')]
    public function show(): Response
    {
        return $this->render('pages\cart\cart.html.twig', ['listAlien' => $this->session->get('listAlien')]);
    }

    #[Route('/add_alien/{id}', name: 'add_alien', methods: ['POST', 'GET'])]
    public function add_alien($id): Response
    {
        $listAlien = $this->session->get('listAlien') ? $this->session->get('listAlien') : [];
        $alien = ['id' => $id];

        if (!in_array($alien, $listAlien)) {
            array_push($listAlien, $alien);
            $this->session->set('listAlien', $listAlien);
            return $this->redirectToRoute('cart', ['listAlien' => $listAlien]);
        }
        return $this->redirectToRoute('cart', ['listAlien' => $listAlien]);
    }

    #[Route('/rmv_alien/{id}', name: 'rmv_alien', methods: ['POST', 'GET'])]
    public function rmv_alien($id): Response
    {
        $listAlien = $this->session->get('listAlien');

        $alien = ['id' => $id];
        $listAlien = array_filter($listAlien, function ($element) use ($alien) {
            return $element !== $alien;
        });

        $this->session->set('listAlien', $listAlien);
        return $this->redirectToRoute('cart', ['listAlien' => $listAlien]);
    }
}

<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;


class LoginController extends AbstractController
{
    private $session;

    public function __construct(private RequestStack $requestStack,)
    {
        $this->session = $requestStack->getSession();
    }

    #[Route('/login', name: 'login')]
    public function show(): Response
    {
        return $this->render('pages\login\login.html.twig');
    }

    #[Route('/login_process', name: 'login_process', methods: ['POST', 'GET'])]
    public function log(EntityManagerInterface $entityManager, Request $request): Response
    {
        $mail = $request->request->get('email');
        $mdp = $request->request->get('password');

        $query = $entityManager->createQuery('
            SELECT u
            FROM App\Entity\Utilisateur u
            WHERE u.mail = :mail
            AND u.mdp = :mdp
        ');

        $query->setParameter('mail', $mail);
        $query->setParameter('mdp', $mdp);

        $users = $query->getResult();

        if ($users) {
            $this->session->set('log', 'true');
            return $this->redirectToRoute('alien_list');
        }
        return $this->redirectToRoute('login');
    }

    #[Route('/logout_process', name: 'logout_process', methods: ['POST', 'GET'])]
    public function logout(): Response
    {
        $this->session->clear();
        return $this->redirectToRoute('login');
    }
}

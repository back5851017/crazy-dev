<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Planete
 *
 * @ORM\Table(name="planete", indexes={@ORM\Index(name="IDX_490E3E57C215AE61", columns={"id_galaxie"})})
 * @ORM\Entity
 */
class Planete
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Galaxie")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_galaxie", referencedColumnName="id")
     * })
     */
    private $idGalaxie;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): static
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getIdGalaxie(): ?int
    {
        return $this->idGalaxie;
    }

    public function setIdGalaxie(?Galaxie $idGalaxie): static
    {
        $this->idGalaxie = $idGalaxie;

        return $this;
    }


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Extraterrestre
 *
 * @ORM\Table(name="extraterrestre", indexes={@ORM\Index(name="IDX_D53DE13397939727", columns={"id_planete"})})
 * @ORM\Entity
 */
class Extraterrestre
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @var int|null
     *
     * @ORM\Column(name="age", type="integer", nullable=true)
     */
    private $age;

    /**
     * @var string|null
     *
     * @ORM\Column(name="espèce", type="string", length=255, nullable=true)
     */
    private $espèce;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Planete")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_planete", referencedColumnName="id")
     * })
     */
    private $idPlanete;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): static
    {
        $this->nom = $nom;

        return $this;
    }

    public function getAge(): ?int
    {
        return $this->age;
    }

    public function setAge(?int $age): static
    {
        $this->age = $age;

        return $this;
    }

    public function getEspèce(): ?string
    {
        return $this->espèce;
    }

    public function setEspèce(?string $espèce): static
    {
        $this->espèce = $espèce;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getIdPlanete(): ?int
    {
        return $this->idPlanete;
    }

    public function setIdPlanete(?Planete $idPlanete): static
    {
        $this->idPlanete = $idPlanete;

        return $this;
    }


}

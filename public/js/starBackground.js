/* 
DOCUMENTATION for Starback library:
https://github.com/zuramai/starback.js
*/

/**
 * The function `getColor` retrieves the value of a CSS variable from the computed style of the document's body element.
 * @param varCss - The `varCss` parameter is a string that represents the name of a CSS variable.
 * @returns the value of the CSS variable specified by the `varCss` parameter.
 */
function getColor(varCss) {
  const style = getComputedStyle(document.body);
  return style.getPropertyValue(`--${varCss}`);
}

/* The `const stars` is an array of objects. Each object represents a star and contains properties such as `id`,
`quantity`, `starSize`, and `starColor`. The `id` property represents the ID of the HTML element that will display the
star. The `quantity` property represents the number of stars to be displayed. The `starSize` property represents the
size of the star. The `starColor` property is assigned the value returned by the `getColor` function, which retrieves
the value of a CSS variable based on the provided parameter. */
const stars = [
  {
    id: "star",
    quantity: 60,
    starSize: 1,
    starColor: getColor("color-text"),
  },
  {
    id: "star2",
    quantity: 20,
    starSize: 1.3,
    starColor: getColor("contrast"),
  },
  {
    id: "star3",
    quantity: 10,
    starSize: 1.7,
    starColor: getColor("primary"),
  },
];

/* The code is iterating over each object in the `stars` array using the `forEach` method. For each object, it creates a
new instance of the `Starback` class and passes in two arguments: the HTML element with the ID specified by the `id`
property of the object, and an object containing various properties. */
stars.forEach((star) => {
  new Starback(document.getElementById(star.id), {
    ...star,
    backgroundColor: ["#00000000"], // transparent
    randomOpacity: true,
    direction: 220,
    starColor: star.starColor || "#ffffff",
  });
});
